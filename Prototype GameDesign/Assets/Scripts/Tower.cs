﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject player;
    public GameObject tower;
    BoxCollider box;

    public GameObject[] unlockElements;

    // Start is called before the first frame update
    void Start()
    {
        box = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if(unlockElements[0] == null && unlockElements[1] == null)
        {
            box.enabled = false;
            Destroy(tower.gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Destroy(player.gameObject);
        }
    }
}
