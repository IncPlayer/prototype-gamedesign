﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Creation de Zone", menuName = "Zone")]
public class ScriptableZone : ScriptableObject
{
    public Material[] material;
}
