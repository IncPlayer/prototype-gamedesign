﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    Rigidbody rb;
    Transform cam;

    [Range(0,10)]
    public float speed;

    [Range(0, 10)]
    public float rotationSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        Vector3 movement = (cam.forward * moveVertical + cam.right * moveHorizontal) * Time.deltaTime * speed;
        movement = new Vector3(movement.x, 0, movement.z);

        rb.MovePosition(rb.position + movement);

        if (movement != Vector3.zero)
        {
            Quaternion direction = Quaternion.LookRotation(movement);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, direction, rotationSpeed);
        }
    }
}
