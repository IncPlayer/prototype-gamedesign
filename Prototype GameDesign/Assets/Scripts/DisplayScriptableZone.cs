﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayScriptableZone : MonoBehaviour
{
    public ScriptableZone scriptableZone;

    public GameObject[] ennemi;

    Renderer rend;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = scriptableZone.material[0];
    }

    // Update is called once per frame
    void Update()
    {
        if (ennemi[0] == null)
        {
            rend.sharedMaterial = scriptableZone.material[1];
        }
    }
}
